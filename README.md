<center>

# <ul>GLaunch</ul> #

### Easily launch gambas recent files via a system TrayIcon ###

Written in Gambas basic by Bruce Steers

</center>

<hr>

## How to Install.. ##

Gambas basic must be installed.\
On debian/ubuntu systems...\
### '# sudo apt-get install gambas3'

(I'm not sure why I am telling you this as this is a program for gambas programmers lol)

Requires the following gambas components
gb.dbus
gb.desktop
gb.settings
gb.gui.trayicon

To Install the application just copy the glaunch.gambas file to your desired location or simply run it from where it resides. The included CompileAndMakeDesktopIcon script will compile the project and create a launcher for you.

### '# sudo apt-get install gambas3-gb-dbus gambas3-gb-desktop gambas3-gb-settings gambas3-gb.gui.trayicon'

## How to compile the application...

Either...

* Load the project into the gambas IDE and hit the "make executable" button to compile the glaunch.gambas file.
* or run the CompileAndMakeDesktopIcon script in the project folder and select to compile and optionally make a launcher.


## How to launch the application...

Manually...\
Just double click the glaunch.gambas file.\
A system tray icon should appear with the gambas logo (with a launcher icon).\


Automatically...\
Add the glaunch.gambas application to your desktop startup.\
On Debian/Ubuntu systems go to menu System/Preferences/Personal/Startup Applications and select "Add" then press the "Browse" button and select the glaunch.gambas file. also give the action a Name.\
IMPORTANT: Adjust the "Delay" to at least 2 seconds or the application can try to start before the desktop has loaded and fails.

    Note: The application has options to make a desktop icon or a system menu launcher and also has an option to enable/disable auto starting with your system

## How to use it...

Left clicking the tray icon will pop up a list of your recent gambas projects.\
Simply click an item to load the gambas IDE with your project

Right click an item to get the following options...
* Execute the projects .gambas file (this option is hidden if no exe exists)
* Execute the project with gbx (use gbx3 to run the project directory, no exe required)
* Open the project directory
* Recompile and make exe (use gbc3 and gba3 to compile the project and make an executable)
* Kill Tasks (Kill all found instances of the task name, hidden if none are found and for glaunch itself) This is usefull if you have problems with an active hung program.


### Right clicking the SystemTray or pseudo icon pops open a menu with the following items.. ###

* Either the recents list or an option to open the list depending on configuration.
* Load the gambas IDE
* Gambas GUI modes. This will launch the gambas IDE using an alternative toolkit like GTK or QT5.
* Options (see below)
* About, display info, version and toolkit
* Check for updates, Check the gitlab repository for a newer Glaunch version and download/install.


Options, this opens the Options window with the following...

#### Program Start ####
* Run at system startup, Add glaunch to the startup applications (should work for most desktops, KDE, GNOME, MATE, CINNAMON, ETC)
* No SysTray, Select if the SystemTray icon should not be used. if selected or if Systray does not exist then a psuedo icon is used.
* Lock pseudo icon, Do not allow the pseudo icon to move.
* Large pseudo icon, Make the pseudo icon double the size.
* Wait for SysTray. Sometimes the SysTray has not initialized before the application starts even with using auto-start delay features, use this to wait up to 60 seconds for the SysTray to be loaded.
* Show recents in menu, right clicking the TrayIcon will also show he recent projects list.
* Opacity slider , set list transparency.

#### Project list ####
* Larger/Smaller icons, change icon size in the list and for the menu on some systems.
* Auto-close , close the list when clicking outside the window.
* Auto Quit, quits the application after selecting an item.
* Limit to last 20, only show the last 20 recent items.
* Show file paths, the list will either show the full path or just the Project name. (the list items icon will always show full path as the tooltip)
* Do not show folder list. do not list all the projects in the most used directory.
* Lock Window, Do not allow the list to be moved/resized.

#### System ####
* Make desktop icon, make a desktop icon to run the application from it's current location.
* Add to system menu, make a menu launcher icon to run the application from it's current location.
* Launcher icon actions. select a glaunch.desktop icon and it will add the selected number of gambas recents to the Actions list. (Actions are right click icon options)

